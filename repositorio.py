from flask import jsonify
from entidades import *
from app import db

HTTP_OK_CREATED = 201
HTTP_OK_REQUEST = 200
HTTP_BAD_REQUEST = 400
HTTP_NOT_FOUND = 404

class RootRepositorio:
    def get(self):
        return jsonify({
            "Servidor": "TP Final Programacion Distribuida I - German Ezequiel Bravo"
        }), HTTP_OK_REQUEST
        
class AutorRepositorio:
    def list(self):
        autores = Autor.query.order_by(Autor.id).all()
    
        return jsonify({
            "autores": [
                {
                    "id": x.id, 
                    "nombre": x.nombre, 
                    "apellido": x.apellido, 
                    "edad": x.edad
                } for x in autores
            ]
        }), HTTP_OK_REQUEST
    
    def get(self, id):
        autor = Autor.query.get(id)
    
        if autor is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
            
        return jsonify({
            "id": autor.id,
            "nombre": autor.nombre,
            "apellido": autor.apellido,
            "edad": autor.edad
        }), HTTP_OK_REQUEST

class GeneroRepositorio:
    def list(self):
        generos = Genero.query.order_by(Genero.id).all()

        return jsonify({
            "generos": [
                {
                    "id": x.id, 
                    "nombre": x.nombre
                } for x in generos
            ]
        }), HTTP_OK_REQUEST
    
    def get(self, id):
        genero = Genero.query.get(id)
    
        if genero is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
            
        return jsonify({
            "id": genero.id,
            "nombre": genero.nombre
        }), HTTP_OK_REQUEST
    
class EditorialRepositorio:
    def list(self):
        editoriales = Editorial.query.order_by(Editorial.id).all()

        return jsonify({
            "editoriales": [
                {
                    "id": x.id, 
                    "nombre": x.nombre
                } for x in editoriales
            ]
        }), HTTP_OK_REQUEST
    
    def get(self, id):
        editorial = Editorial.query.get(id)
    
        if editorial is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
        
        return jsonify({
            "id": editorial.id,
            "nombre": editorial.nombre
        }), HTTP_OK_REQUEST

class LibroRepositorio:
    def list(self):
        libros = Libro.query.order_by(Libro.id).all()
    
        return jsonify({
            "libros": [
                {
                    "id": x.id,
                    "titulo": x.titulo,
                    "autorId": x.autorId,
                    "generoId": x.generoId,
                    "editorialId": x.editorialId
                } for x in libros
            ]
        }), HTTP_OK_REQUEST
        
    def get(self, id):
        libro = Libro.query.get(id)
    
        if libro is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
            
        return jsonify({
            "id": libro.id, 
            "titulo": libro.titulo,
            "autorId": libro.autorId,
            "generoId": libro.generoId,
            "editorialId": libro.editorialId
        }), HTTP_OK_REQUEST
    
    def create(self):
        from flask import request
        json = request.get_json()
        titulo = json.get("titulo")
        autorId = json.get("autorId")
        generoId = json.get("generoId")
        editorialId = json.get("editorialId")
        
        newLibro = Libro(titulo, autorId, generoId, editorialId)

        db.session.add(newLibro)

        try:
            db.session.commit()
        except Exception as e:
            return jsonify({
                "Resultado": str(e)
            }), HTTP_BAD_REQUEST
            
        return jsonify({
            "id": newLibro.id
        }), HTTP_OK_CREATED
    
    def update(self, id):
        libro = Libro.query.get(id)
    
        if libro is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
        
        columnas = libro.__table__.columns.keys()[1:] # Se seleccionan las columnas que nos interesan, es decir todas menos "id" (0) pues no se puede o debe actualizar ya que corresponde como identificador al recurso
        
        from flask import request
        json = request.get_json()
        
        for columna in columnas:
            setattr(libro, columna, json.get(columna))
                
        db.session.add(libro)

        try:
            db.session.commit()
        except Exception as e:
            return jsonify({
                "Resultado": str(e)
            }), HTTP_BAD_REQUEST
            
        return jsonify({
            "id": libro.id
        }), HTTP_OK_REQUEST
    
    def delete(self, id):
        libro = Libro.query.get(id)
    
        if libro is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
                
        db.session.delete(libro)

        try:
            db.session.commit()
        except Exception as e:
            return jsonify({
                "Resultado": str(e)
            }), HTTP_BAD_REQUEST
            
        return jsonify({
            "id": libro.id
        }), HTTP_OK_REQUEST

class SocioRepositorio:
    def list(self):
        socios = Socio.query.order_by(Socio.id).all()
    
        return jsonify({
            "socios": [
                {
                    "id": x.id,
                    "nombre": x.nombre,
                    "apellido": x.apellido,
                    "edad": x.edad,
                } for x in socios
            ]
        }), HTTP_OK_REQUEST
    
    def get(self, id):
        socio = Socio.query.get(id)
    
        if socio is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
            
        return jsonify({
            "id": socio.id,
            "nombre": socio.nombre,
            "apellido": socio.apellido,
            "edad": socio.edad
        }), HTTP_OK_REQUEST
    
    def create(self):
        from flask import request
        json = request.get_json()
        nombre = json.get("nombre")
        apellido = json.get("apellido")
        edad = json.get("edad")
        
        newSocio = Socio(nombre, apellido, edad)

        db.session.add(newSocio)

        try:
            db.session.commit()
        except Exception as e:
            return jsonify({
                "Resultado": str(e)
            }), HTTP_BAD_REQUEST
            
        return jsonify({
            "id": newSocio.id
        }), HTTP_OK_CREATED
    
    def update(self, id):
        socio = Socio.query.get(id)
    
        if socio is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
        
        columnas = socio.__table__.columns.keys()[1:] # Se seleccionan las columnas que nos interesan, es decir todas menos "id" (0) pues no se puede o debe actualizar ya que corresponde como identificador al recurso
        
        from flask import request
        json = request.get_json()
        
        for columna in columnas:
            setattr(socio, columna, json.get(columna))
                
        db.session.add(socio)

        try:
            db.session.commit()
        except Exception as e:
            return jsonify({
                "Resultado": str(e)
            }), HTTP_BAD_REQUEST
            
        return jsonify({
            "id": socio.id
        }), HTTP_OK_REQUEST
    
    def delete(self, id):
        socio = Socio.query.get(id)
    
        if socio is None:
            return jsonify({
                "Resultado": "No encontrado"
            }), HTTP_NOT_FOUND
        
        db.session.delete(socio)
        
        try:
            db.session.commit()
        except Exception as e:
            return jsonify({
                "Resultado": str(e)
            }), HTTP_BAD_REQUEST
            
        return jsonify({
            "id": socio.id
        }), HTTP_OK_REQUEST