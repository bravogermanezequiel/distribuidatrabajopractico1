from flask import request, Blueprint
from repositorio import *

ruta = Blueprint("rutas", __name__, url_prefix="")

@ruta.route("/", methods=["GET"])
def server_info():
    return RootRepositorio().get()

@ruta.route("/autores/", methods=["GET"])
def listAutores():
    return AutorRepositorio().list()

@ruta.route("/autor/<int:id>", methods=["GET"])
def getAutor(id):
    return AutorRepositorio().get(id)
    
@ruta.route("/generos/", methods=["GET"])
def listGeneros():
    return GeneroRepositorio().list()

@ruta.route("/genero/<int:id>", methods=["GET"])
def getGenero(id):
    return GeneroRepositorio().get(id)
    
@ruta.route("/editoriales/", methods=["GET"])
def listEditoriales():
    return EditorialRepositorio().list()

@ruta.route("/editorial/<int:id>", methods=["GET"])
def getEditorial(id):
    return EditorialRepositorio().get(id)

@ruta.route("/libros/", methods=["GET"])
def listLibros():
    return LibroRepositorio().list()

@ruta.route("/libro/<int:id>", methods=["GET"])
def getLibro(id):
    return LibroRepositorio().get(id)
    
@ruta.route("/libro/", methods=["POST"])
def createLibro():
    return LibroRepositorio().create()

@ruta.route("/libro/<int:id>", methods=["PUT"])
def updateLibro(id):
    return LibroRepositorio().update(id)
    
@ruta.route("/libro/<int:id>", methods=["DELETE"])
def deleteLibro(id):
    return LibroRepositorio().delete(id)

@ruta.route("/socios/", methods=["GET"])
def listSocios():
    return SocioRepositorio().list()

@ruta.route("/socio/<int:id>", methods=["GET"])
def getSocio(id):
    return SocioRepositorio().get(id)
    
@ruta.route("/socio/", methods=["POST"])
def createSocio():
    return SocioRepositorio().create()

@ruta.route("/socio/<int:id>", methods=["PUT"])
def updateSocio(id):
    return SocioRepositorio().update(id)

@ruta.route("/socio/<int:id>", methods=["DELETE"])
def deleteSocio(id):
    return SocioRepositorio().delete(id)
