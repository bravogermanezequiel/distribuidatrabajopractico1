# Trabajo Practico Nº1 - Programación Distribuida II

<h2>Bienvenido</h2>
<p>Este proyecto forma parte del trabajo práctico llevado a cabo sobre la materia Programación Distribuida II, en la cual se adopto el trabajo práctico final de la materia Programación Distribuida I, y se le realizo integraciones de tests, mejoras de código limpio, etc.</p>
<p>El mismo consta de un sistema de administración de libros y socios para una libreria.</p>
<h2>Instalación:</h2>
<pre>git clone https://gitlab.com/bravogermanezequiel/distribuidatrabajopractico1
cd distribuidatrabajopractico1
pip3 install -r requirements.txt</pre>

<h2>Como iniciar el servicio web:</h2>
<pre>python3 server.py</pre>
<p>A partir de este momento debemos tener nuestra API escuchando en http://localhost:3000</p>

<h2>Como ejecutar los test:</h2>
<pre>python3 -m unittest</pre>

<h2>De que dispone la API</h2>

<b>La API dispone de las siguientes entidades:</b>
<ul>
    <li>
        Autor
        <ol>
            <li>id</li>
            <li>nombre</li>
            <li>apellido</li>
            <li>edad</li>
        </ol>
    </li>
    <li>
        Genero
        <ol>
            <li>id</li>
            <li>nombre</li>
        </ol>
    </li>
    <li>
        Editorial
        <ol>
            <li>id</li>
            <li>nombre</li>
        </ol>
    </li>
    <li>
        Libro
        <ol>
            <li>id</li>
            <li>titulo</li>
            <li>autorId</li>
            <li>generoId</li>
            <li>editorialId</li>
        </ol>
    </li>
    <li>
        Socio
        <ol>
            <li>id</li>
            <li>nombre</li>
            <li>apellido</li>
            <li>edad</li>
        </ol>
    </li>
</ul>

<h2>¿Que operaciones se pueden realizar?</h2>

<ul>
    <li>
        Autor
        <ol>
            <li>Obtencion listado de autores: GET /autores</li>
            <li>Obtencion por autor: GET /autor/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Genero
        <ol>
            <li>Obtencion listado de generos: GET /generos</li>
            <li>Obtencion por genero: GET /genero/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Editorial
        <ol>
            <li>Obtencion listado de editoriales: GET /editoriales</li>
            <li>Obtencion por editorial: GET /editorial/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Libro
        <ol>
            <li>Obtencion listado de libros: GET /libros</li>
            <li>Obtencion por libro: GET /libro/&lt;int:id&gt;</li>
            <li>Creación de libro: POST /libro</li>
            <li>Actualización de libro: PUT /libro/&lt;int:id&gt;</li>
            <li>Borrado de libro: DELETE /libro/&lt;int:id&gt;</li>
        </ol>
    </li>
    <li>
        Socio
        <ol>
            <li>Obtencion listado de socios: GET /socios</li>
            <li>Obtencion por socio: GET /socio/&lt;int:id&gt;</li>
            <li>Creación de socio: POST /socio</li>
            <li>Actualización de socio: PUT /lisociobro/&lt;int:id&gt;</li>
            <li>Borrado de socio: DELETE /socio/&lt;int:id&gt;</li>
        </ol>
    </li>
</ul>
