from . import BaseTestClass
from app import db
from entidades import Socio
import json

class SociosTestCase(BaseTestClass):
    
    def testListSocios(self):
        with self.app.app_context():
            socio1Data = {"nombre": "German", "apellido": "Von Kurgen", "edad": 70}
            socio1 = Socio(**socio1Data)
            db.session.add(socio1)
            
            socio2Data = {"nombre": "Liver", "apellido": "Mange", "edad": 35}
            socio2 = Socio(**socio2Data)
            db.session.add(socio2)
            
            db.session.commit()
            
            res = self.client.get('/socios/')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData["socios"][0].get("nombre") == socio1Data.get("nombre")
            assert jsonData["socios"][0].get("apellido") == socio1Data.get("apellido")
            assert jsonData["socios"][0].get("edad") == socio1Data.get("edad")
            assert jsonData["socios"][1].get("nombre") == socio2Data.get("nombre")
            assert jsonData["socios"][1].get("apellido") == socio2Data.get("apellido")
            assert jsonData["socios"][1].get("edad") == socio2Data.get("edad")

    def testGetDefinedSocio(self):
        with self.app.app_context():
            socio1Data = {"nombre": "German", "apellido": "Von Kurgen", "edad": 70}
            socio1 = Socio(**socio1Data)
            db.session.add(socio1)
            
            db.session.commit()
            
            res = self.client.get('/socio/1')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 200
            assert jsonData.get("id") == 1
            assert jsonData.get("nombre") == socio1Data.get("nombre")
            assert jsonData.get("apellido") == socio1Data.get("apellido")
            assert jsonData.get("edad") == socio1Data.get("edad")
    
    def testGetUndefinedSocio(self):
        with self.app.app_context():
            socio1Data = {"nombre": "German", "apellido": "Von Kurgen", "edad": 70}
            socio1 = Socio(**socio1Data)
            db.session.add(socio1)
            
            db.session.commit()
            
            res = self.client.get('/socio/10')
            
            jsonData = json.loads(res.data.decode())
            
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
    
    def testCreateSocio(self):
        with self.app.app_context():
            socio1Data = {"nombre": "German", "apellido": "Von Kurgen", "edad": 70}
            
            res = self.client.post('/socio/', data=json.dumps(socio1Data), content_type='application/json')
            
            assert res.status_code == 201
            
            jsonData = json.loads(res.data.decode())
            assert jsonData.get("id") == 1
            
            socio = Socio.query.get(1)
            assert socio.nombre == socio1Data.get("nombre")
            assert socio.apellido == socio1Data.get("apellido")
            assert socio.edad == socio1Data.get("edad")
    
    def testUpdateDefinedSocio(self):
        with self.app.app_context():
            socio1Data = {"nombre": "German", "apellido": "Von Kurgen", "edad": 70}
            socio1 = Socio(**socio1Data)
            db.session.add(socio1)
            
            newSocio1Data = {"nombre": "Gilbert", "apellido": "McKenzie", "edad": 24}
            
            res = self.client.put('/socio/1', data=json.dumps(newSocio1Data), content_type='application/json')
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 200
            assert jsonData.get("id") == 1
            
            socio = Socio.query.get(1)
            assert socio.nombre == newSocio1Data.get("nombre")
            assert socio.apellido == newSocio1Data.get("apellido")
            assert socio.edad == newSocio1Data.get("edad")
    
    def testUpdateUndefinedSocio(self):
        with self.app.app_context():
            socio1Data = {"nombre": "German", "apellido": "Von Kurgen", "edad": 70}
            
            res = self.client.put('/socio/1', data=json.dumps(socio1Data), content_type='application/json')
            
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
    
    def testDeleteDefinedSocio(self):
        with self.app.app_context():
            socio1Data = {"nombre": "German", "apellido": "Von Kurgen", "edad": 70}
            socio1 = Socio(**socio1Data)
            db.session.add(socio1)
            
            res = self.client.delete('/socio/1')
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 200
            assert jsonData.get("id") == 1

    def testDeleteUndefinedSocio(self):
        with self.app.app_context():            
            res = self.client.delete('/socio/1')
            
            jsonData = json.loads(res.data.decode())
            assert res.status_code == 404
            assert jsonData.get("Resultado") == "No encontrado"
