from sqlalchemy.engine import Engine
from sqlalchemy import event
from flask import Flask, jsonify
from flask_sqlalchemy import SQLAlchemy
import os

db = SQLAlchemy()

def dummyPopulate(db):
    autores = [Autor("Isaac", "Asimov", 89), Autor("Peter", "Sczaskiw", 72), Autor("Rubin", "Persel", 52)]
    for autor in autores:
        db.session.add(autor)
        db.session.commit()
    
    generos = [Genero("Policial"), Genero("Ciencia ficcion"), Genero("Romantico")]
    for genero in generos:
        db.session.add(genero)
        db.session.commit()
    
    editoriales = [Editorial("Nova"), Editorial("Larousse"), Editorial("Librito S.A.")]
    for editorial in editoriales:
        db.session.add(editorial)
        db.session.commit()

'''
    Decorator de listener de evento "connect". 
    Necesario para setear las constraints respecto a las Foreign Keys para que ante una eventual excepcion por una foreign key invalida, el SQLite responda con un throw Exception.
'''
@event.listens_for(Engine, "connect")
def setSQLitePragma(dbAPIConnection, connectionRecord):
    cursor = dbAPIConnection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()
    
def initApp(config="Config"):
    from verbos import ruta

    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(f"config.{config}")
    app.register_blueprint(ruta)
    
    dbExists = os.path.isfile('./db/sqlite.db')
    # Si la tabla SQLite no existia antes de llamar al metodo create_all, cargo la dummy data basica para poder interactuar con la API sin entrar en excepciones constantes.
    if not dbExists:
        dummyPopulate(db)
        
    db.init_app(app)
    
    return app